#!/bin/bash

datafind_output=$1 # The output file of URLs from datafind
read_frames_input_pattern=$2 # produce this file pattern with space-separated list of local PFNs
njobs=$3
urltype=$4
num_urls=$5
token_str=$6

for idx in `seq 0 $((njobs-1))`; do 

	read_frames_input="${read_frames_input_pattern}:${idx}.pfns"

	if [[ ${urltype} = osdf ]]; then

		transfer_urls=$(echo ${read_frames_input} | sed 's/pfns/urls/')  # make this file of comma-separated OSDF URLs for transfer_input_files

		shuf -n ${num_urls} ${datafind_output} | tr '\n' ',' | sed 's/,$/\n/g' | tee ${transfer_urls} | tr ',' '\n' | xargs -n1 basename | tr '\n' ' ' > ${read_frames_input}

		if [ ! -z ${token_str} ]; then
			sed -i "s|osdf://|${token_str}+osdf://|g" ${transfer_urls}
		fi

	elif [[ ${urltype} = file ]]; then 
		shuf -n ${num_urls} ${datafind_output} | sed 's|file://localhost||' | tr '\n' ' ' > ${read_frames_input}
	else
		echo "value error"
	fi

done
