# IGWN Pool Exorciser

Second generation utility to exercise and test IGWN pool infrastructure,
services and resource availability.

The goal is to create a more generic, crondor-based job submission
package using HTCondor python bindings to dynamically create and submit test
workflows on a schedule from a shared account.

## Project structure

- `demo`: prototype scripts, HTCondor job/workflow descriptions.
- `apps`: source, scripts, executables which will run as HTCondor jobs in the
  IGWN pool.
- `pool-exorciser`: installable python package to set up and generate HTCondor workflows.

## Development notes

Desired usage:

```
$  igwn_exorciser --config-file=config.yaml prepare-crondor --parent-directory ~osg.test/public_html/igwn-exorciser
...Parsing config.yaml...
...Setting up crondor job for test: cpu-heartbeats...
...Setting up crondor job for test: gpu-heartbeats...
# etc
```

Creates a crondor job for each test enumerated in `config.yaml` with:

```
executable = igwn_exorciser
arguments = exorcise -e stress-ng -m cpu --executable-arguments '--vm 1 --vm-bytes 2G --timeout 1800s --verbose' --submit-dag
```

where the `exorcise` subcommand calls out to methods similar to
`[generate_heartbeat_dags](https://git.ligo.org/computing/distributed/igwn-pool-exorciser/-/blob/main/demos/generate_heartbeat_dags)`
but where the submit file is created from the contents of `config.yaml` instead of hard-coded into the source itself.

So prototype for the `config.yaml` is like:

```
cpu-access-test
  crondor:
    submit-file: cpu-access-crondor.sub
    executable: igwn_exorcisor
    arguments: exorcise --executable stress-ng --mode cpu --executable-arguments '--vm 1 --vm-bytes 2G --timeout 1800s --verbose' --submit-dag
    cron_hour: "*/1"
    request_memory: "100 MB"
    ...
  test:
    dag-file: cpu-access-test.dag
    submit-file: cpu-access-test.sub
    request_memory: "100 MB"
    sites: "all"
    njobs-per-site: 5
```

