#!/bin/bash

# Set Cupy cache location to somewhere we have write access
# https://docs.cupy.dev/en/stable/reference/environment.html
export CUPY_CACHE_IN_MEMORY=0
CUPY_CACHE_DIR=.cupy/kernel_cache
if [[ -d ${HOME} && -w ${HOME} ]]; then
  export CUPY_CACHE_DIR="${HOME}/${CUPY_CACHE_DIR}"
elif [[ -d ${TMPDIR} && -w ${TMPDIR} ]]; then
  export CUPY_CACHE_DIR="${TMPDIR}/${CUPY_CACHE_DIR}"
elif [[ -d /tmp && -w /tmp ]]; then
  export CUPY_CACHE_DIR="/tmp/${CUPY_CACHE_DIR}"
else
  echo "Found no writable locations for CUPY_CACHE_DIR"
  exit 1
fi
mkdir -p ${CUPY_CACHE_DIR}

exec "$@"

# Clean up
rm -rf ${CUPY_CACHE_DIR}
