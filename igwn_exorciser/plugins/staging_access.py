#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""staging_access plugin for IGWN pool exorciser.  Genearetes a DAG with N parent data upload
jobs and N data-download jobs.  A FINAL node cleans up the staging origin directory on DAG exit.
"""
import os
import uuid
from pathlib import Path

from .. import utils

def make_dag(dag_config: dict, submit_descriptions: list, parent_directory: str, vo: str = "IGWN",
             ) -> utils.dags.DAG:
    """Generate a flat DAGMan workflow

    @param dag_config: dictionary with configuration parameters parsed from YAML.
    @param submit_descriptions: a list of independent HTCondor job submit descriptions.
    @param parent_directory: desired output directory to write DAG and submit files to.
    @return: a DAGMan workflow with a single layer
    @rtype: utils.dags.DAG
    """

    assert "sites" in dag_config, "DAG configuration for Heartbeats plugin must specify sites"

    # Where to submit jobs to:
    unique_glidein_sites = utils.get_sites(mode=dag_config["mode"], vo=vo)
    if dag_config["sites"] == "all":
        assert "njobs-per-site" in dag_config, "Need to specify njobs-per-site with sites=all"
        glidein_sites = dag_config["njobs-per-site"] * unique_glidein_sites
    elif dag_config["sites"] == "any":
        assert "njobs" in dag_config, "Need to specify njobs-per-site with sites=any"
        glidein_sites = [','.join(unique_glidein_sites) for _ in range(dag_config["njobs"])]
    else:
        raise ValueError("dag_config['sites'] must be one of 'all', 'any'")

    # Get the output directory name
    try:
        sandbox_data_dir_pfx = dag_config['sandbox-data-dir']
    except KeyError:
        sandbox_data_dir_pfx = "staging-data-" + os.path.basename(parent_directory)

    # --- Generate vars: upload and download jobs have a 1-1 mapping so generate vas together
    print(f"Generating vars for {(len(glidein_sites))} OSDF upload and {(len(glidein_sites))} "
          f"OSDF (data download/verification) jobs")

    # Token-handle handling
    if dag_config["token-str"] == "scitokens":

        # Use default in generate staging data
        output_destination = dag_config["output-destination"]
        output_url = ""

    else:

        if "token-handles" in dag_config:

            # Writing
            assert "write" in dag_config["token-handles"], "Must specify token write handle" 
            write_token_url_pfx = \
                    f'{dag_config["token-str"]}.{dag_config["token-handles"]["write"]}'

            assert "read" in dag_config["token-handles"], "Must specify token read handle" 
            read_token_url_pfx = \
                    f'{dag_config["token-str"]}.{dag_config["token-handles"]["read"]}'


        else:

            write_token_url_pfx = dag_config["token-str"]
            read_token_url_pfx = dag_config["token-str"]


        output_destination = \
                f'{write_token_url_pfx}+{dag_config["output-destination"]}'

        # Add the token prefix to the OSDF url when writing the lists of URLs
        # for file transfer in verify_staging_data (download-from-osdf.sub)
        output_url = f'{read_token_url_pfx}+{dag_config["output-destination"]}'


    upload_vars = []
    download_vars = []
    for glidein_site_choice in glidein_sites:

        sandbox_data_dir = f"{sandbox_data_dir_pfx}-{uuid.uuid4()}"

        # --- Data upload vars
        upload_vars.append(
            {"sandbox_data_dir": f"{sandbox_data_dir}",
             "output_destination": output_destination,
             "DESIRED_Sites": glidein_site_choice,
             "n_files": dag_config["n-files"],
             "mb_per_file": dag_config["mb-per-file"],
             "token_str": dag_config["token-str"],
             "output_url": output_url
             }
        )

        # --- Data verification/download vars
        download_vars.append(
            {"output_destination": dag_config["output-destination"],
             "DESIRED_Sites": glidein_site_choice,
             "token_str": dag_config["token-str"],
             "transfer_urls": f'{dag_config["output-local-path"]}/{sandbox_data_dir}.urls'
             }
        )

    # Make DAG
    dag = utils.dags.DAG()

    # --- Data upload layer

    # Horrible hack to get the right array
    upload_job_name = utils.find_description(submit_descriptions, job_name_pattern="upload_data")
    upload_layer = dag.layer(
        name = "upload-to-osdf",
        submit_description = submit_descriptions[upload_job_name],
        dir = parent_directory,
        vars = upload_vars
    )

    # --- Data verification/download layer
    # Horrible hack to get the right array
    download_job_name = utils.find_description(submit_descriptions,
                                               job_name_pattern="download_data")


    # Convert to string description and add an explicit queue statement to get the
    # URLs for file transfer
    desc_str = str(submit_descriptions[download_job_name]) + \
               "queue TRANSFER_URLS from $(transfer_urls)\n"

    # Convert back to submit description
    submit_descriptions[download_job_name] = utils.htcondor.Submit(desc_str)

    try:
        submit_descriptions[download_job_name]["transfer_input_files"] = \
            submit_descriptions[download_job_name]["transfer_input_files"] + ", $(TRANSFER_URLS)"
    except KeyError:
        submit_descriptions[download_job_name]["transfer_input_files"] = "$(TRANSFER_URLS)"

    # Write out the sub file myself to preseve my queue from statement
    if not os.path.exists(parent_directory):
        os.makedirs(parent_directory)

    with open(Path(f"{parent_directory}/download-from-osdf.sub"), 'w') as sub_file:
        sub_file.writelines(str(submit_descriptions[download_job_name]))

    _ = upload_layer.child_layer(
        name = "download-from-osdf",
        submit_description = Path(f"{parent_directory}/download-from-osdf.sub"),
        dir=parent_directory,
        vars=download_vars,
        edge=utils.dags.OneToOne()
    )

    # --- FINAL node to aggregate hold reaons

    try:
        # Get and write submit description (normally handled by the node layer)
        aggregate_job_name = utils.find_description(submit_descriptions, job_name_pattern="aggregate.*")

    except AssertionError:

        pass

    else:

        with open(Path(f"{parent_directory}/aggregate-hold-reasons.sub"), 'w') as sub_file:
            sub_file.writelines(str(submit_descriptions[aggregate_job_name]) + "queue\n")

        _ = dag.final(name="aggregate-hold-reasons",
                      submit_description=Path(f"{parent_directory}/aggregate-hold-reasons.sub"),
                      dir=Path(parent_directory))


    return dag
