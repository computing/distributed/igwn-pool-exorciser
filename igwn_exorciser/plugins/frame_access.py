#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Data access plugin for IGWN pool exorciser.
"""

import os
from pathlib import Path
import re
from .. import utils


def make_dag(dag_config: dict, submit_descriptions: list, parent_directory: str, vo: str = "IGWN",
             ) -> utils.dags.DAG:
    """Generate a data access workflow with a single datafind parent node and many

    @param dag_config: dictionary with configuration parameters parsed from YAML.
    @param submit_descriptions: a list of independent HTCondor job submit descriptions.
    @param parent_directory: desired output directory to write DAG and submit files to.
    @return: a DAGMan workflow with a single parent datafind node and many child nodes to read
    files.
    @rtype: utils.dags.DAG
    """

    # Get sites
    assert "sites" in dag_config, "DAG configuration for Heartbeats plugin must specify sites"

    assert dag_config["urltype"] in ["osdf", "file"], "urltype must be osdf or cvmfs"

    # Where to submit jobs to:
    unique_glidein_sites = utils.get_sites(mode=dag_config["mode"], vo=vo)
    if dag_config["sites"] == "all":
        assert "njobs-per-site" in dag_config, "Need to specify njobs-per-site with sites=all"
        glidein_sites = dag_config["njobs-per-site"] * unique_glidein_sites
    elif dag_config["sites"] == "any":
        assert "njobs" in dag_config, "Need to specify njobs-per-site with sites=any"
        glidein_sites = [','.join(unique_glidein_sites) for _ in range(dag_config["njobs"])]
    else:
        raise ValueError("dag_config['sites'] must be one of 'all', 'any'")

    # Modifications to read-frames job description for stuff that won't go in yaml
    for desc in submit_descriptions:
        if ("read-frames" in desc) and ("osdf" in dag_config["urltype"]):

            # Convert to string description and add an explicit queue statement to get the
            # URLs for file transfer
            desc_str = str(submit_descriptions[desc]) + \
                       "queue TRANSFER_URLS from $(transfer_urls)\n"

            # Convert back to submit description
            submit_descriptions[desc] = utils.htcondor.Submit(desc_str)

            try:
                submit_descriptions[desc]["transfer_input_files"] = \
                    submit_descriptions[desc]["transfer_input_files"] + ", $(TRANSFER_URLS)"
            except KeyError:
                submit_descriptions[desc]["transfer_input_files"] = "$(TRANSFER_URLS)"

    # --- Generate datafind layer
    datafind_vars = [{
        "observatory": dag_config["observatory"],
        "frametype": dag_config["frametype"],
        "gpsstart": dag_config["gpsstart"],
        "gpsend": dag_config["gpsend"] ,
        "urltype": dag_config["urltype"],
        "datafind_output": dag_config["datafind-output"],
        "token_str": dag_config["token-str"]
    }]

    # Get the right submit description
    datafind_key = utils.find_description(submit_descriptions, "datafind")
    datafind_sub = submit_descriptions[datafind_key]

    dag = utils.dags.DAG()

    # Write POST script to select frames for each downstream job
    write_post_script(parent_directory)
    post_script_arguments = ["$RETURN",
                             dag_config["datafind-output"],
                             dag_config["read-frames-input-pattern"],
                             len(glidein_sites),
                             dag_config["urltype"],
                             dag_config["num-urls"],
                             dag_config["token-str"]]

    try:
        post_script_arguments.append(dag_config["token-handle"])
    except KeyError:
        pass


    datafind_layer = dag.layer(
        name = "datafind",
        submit_description = datafind_sub,
        dir=parent_directory,
        vars = datafind_vars,
        **{"post": utils.dags.Script(
            executable=f"{parent_directory}/parse-datafind-urls.sh",
            arguments=post_script_arguments)}
    )

    # --- Generate data access layer
    # Get the number of jobs to generate from njobs/njobs-per-site

    read_frames_vars = []

    print(f"Generating vars for {len(glidein_sites)} jobs")
    for idx, glidein_site_choice in enumerate(glidein_sites):

        if "osdf" == dag_config["urltype"]:
            read_frames_vars.append(
                {"DESIRED_Sites": glidein_site_choice,
                 "pfn_list": f"{dag_config['read-frames-input-pattern']}:{idx}.pfns",
                 "transfer_urls": f"{dag_config['read-frames-input-pattern']}:{idx}.urls",
                 "token_str": dag_config["token-str"]
                 }
            )
        elif "file" == dag_config["urltype"]:
            read_frames_vars.append(
                {"DESIRED_Sites": glidein_site_choice,
                 "pfn_list": f"{dag_config['read-frames-input-pattern']}:{idx}.pfns",
                 "token_str": dag_config["token-str"]
                 }
            )


    # Get the right submit description
    read_frames_key = utils.find_description(submit_descriptions, "read-frames")
    read_frames_sub = submit_descriptions[read_frames_key]

    # Write out the sub file myself to preseve my queue from statement
    if dag_config["urltype"] == "osdf":
        sub_file_path = Path(f"{parent_directory}/read-frames.sub")
        with open(sub_file_path, 'w') as sub_file:
            sub_file.writelines(str(read_frames_sub))

        _ = datafind_layer.child_layer(
            name="read-frames",
            dir=parent_directory,
            submit_description=sub_file_path,
            vars=read_frames_vars,
        )

    else:

        _ = datafind_layer.child_layer(
            name="read-frames",
            dir=parent_directory,
            submit_description=read_frames_sub,
            vars=read_frames_vars,
        )

    # --- FINAL node to aggregate hold reaons

    try:
        # Get and write submit description (normally handled by the node layer)
        aggregate_job_name = utils.find_description(submit_descriptions, job_name_pattern="aggregate.*")

    except AssertionError:

        pass

    else:

        with open(Path(f"{parent_directory}/aggregate-hold-reasons.sub"), 'w') as sub_file:
            sub_file.writelines(str(submit_descriptions[aggregate_job_name]) + "queue\n")

        _ = dag.final(name="aggregate-hold-reasons",
                      submit_description=Path(f"{parent_directory}/aggregate-hold-reasons.sub"),
                      dir=Path(parent_directory))

    return dag


def write_post_script(parent_directory):
    """Generate a POST script to manipulate datafind output to input for transfer_input_files
    etc"""

    post_str="""#!/bin/bash -e

datafind_return_val=$1
datafind_output=$2 # The output file of URLs from datafind
read_frames_input_pattern=$3 # produce this file pattern with space-separated list of local PFNs
njobs=$4
urltype=$5
num_urls=$6
token_str=$7
token_handle=$8

if [ "$datafind_return_val" -ne 0 ]; then
    echo "Datafind failed"
    exit 1
fi

for idx in `seq 0 $((njobs-1))`; do 

    read_frames_input="${read_frames_input_pattern}:${idx}.pfns"

    if [[ ${urltype} = osdf ]]; then

        transfer_urls=$(echo ${read_frames_input} | sed 's/pfns/urls/')  # make this file of comma-separated OSDF URLs for transfer_input_files

        shuf -n ${num_urls} ${datafind_output} | tr '\\n' ',' | sed 's/,$/\\n/g' | tee ${transfer_urls} | tr ',' '\\n' | xargs -n1 basename | tr '\\n' ' ' > ${read_frames_input}

                if [ ! -z ${token_handle} ]; then
                    sed -i "s|osdf://|${token_str}.${token_handle}+osdf://|g" ${transfer_urls}
                else
                    sed -i "s|osdf://|${token_str}+osdf://|g" ${transfer_urls}
                fi

    elif [[ ${urltype} = file ]]; then 
        shuf -n ${num_urls} ${datafind_output} | sed 's|file://localhost||' | tr '\\n' ' ' > ${read_frames_input}
    else
        echo "value error"
    fi

done
"""

    if not os.path.exists(parent_directory):
        os.makedirs(parent_directory)

    script_pfn = f"{parent_directory}/parse-datafind-urls.sh"
    print(f"Writing POST script to {script_pfn}")
    with open(script_pfn, 'w') as datafind_parser:
        datafind_parser.write(post_str)
    os.chmod(script_pfn, 0o755)
