#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Heartbeats plugin for IGWN pool exorciser.  Genearetes a flat DAG with a fixed number of jobs
targeting every site listed in the OSG SDSC and Cern factories as supporting LIGO or Virgo VO jobs.
"""

from .. import utils


def make_dag(dag_config: dict, submit_descriptions: list, parent_directory: str, vo: str = "IGWN",
             ) -> utils.dags.DAG:
    """Generate a flat DAGMan workflow

    @param dag_config: dictionary with configuration parameters parsed from YAML.
    @param submit_descriptions: a list of independent HTCondor job submit descriptions.
    @param parent_directory: desired output directory to write DAG and submit files to.
    @return: a DAGMan workflow with a single layer
    @rtype: utils.dags.DAG
    """

    assert "sites" in dag_config, "DAG configuration for Heartbeats plugin must specify sites"

    unique_glidein_sites = utils.get_sites(mode=dag_config["mode"], vo=vo)
    if dag_config["sites"] == "all":
        assert "njobs-per-site" in dag_config, "Need to specify njobs-per-site with sites=all"
        glidein_sites = dag_config["njobs-per-site"]*unique_glidein_sites
        submit_vars = [{'DESIRED_Sites': site} for site in glidein_sites]
    elif dag_config["sites"] == "any":
        assert "njobs" in dag_config, "Need to specify njobs-per-site with sites=any"
        submit_vars = [{'DESIRED_Sites': ','.join(unique_glidein_sites)} for _ in
                       range(dag_config["njobs"])]
    else:
        raise ValueError("dag_config['sites'] must be one of 'all', 'any'")

    dag = utils.flat_dag(submit_descriptions, submit_vars, parent_directory)

    return dag
