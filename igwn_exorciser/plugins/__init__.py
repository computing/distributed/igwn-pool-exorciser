# -*- coding: utf-8 -*-
# Copyright James Alexander Clark 2024
#
# This file is part of igwn_exorciser.
#
# GWRUcio is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GWDataFind is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Plugins to generate DAGs for igwn pool exorciser tests
"""

__author__ = 'James Alexander Clark <james.clark@ligo.org>'
__version__ = '0.0.0'


import importlib
import pkgutil


def load_plugins():
    plugins = {}
    for importer, modname, ispkg in pkgutil.iter_modules([__path__[0]]):
        full_name = __package__ + "." + modname
        module = importlib.import_module(full_name)
        plugins[modname] = module
    return plugins

