# -*- coding: utf-8 -*-
# Copyright (C) 2021  James Alexander Clark <james.clark@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Utility to generate HTCondor test workflows to exercise/exorcise the IGWN dHTC pool.
"""

import argparse
import os
import sys

from yaml import load

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from datetime import datetime

import argcomplete

from . import prepare_workflows

__author__ = 'James Alexander Clark <james.clark@ligo.org>'


def parse_config_yaml(config_file: str) -> dict:
    """

    @param config_file: path to config file
    @return: dictionary with configuration information parsed from YAML
    @rtype: object
    """
    now = datetime.now().strftime("%Y-%m-%d %H:%M")
    print(f"----------------- {now} ---------------------")
    print(f"Parsing configuration from {os.path.abspath(config_file)}...")
    with open(os.path.abspath(config_file), 'r') as config_stream:
        config_data = load(config_stream, Loader=Loader)
    return config_data


def parse_inputs() -> argparse.Namespace:
    """Command line parser
    @return: parsed arguments in an argparser object
    @rtype: argparse.Namespace
    """

    aparser = argparse.ArgumentParser(description=__doc__)

    # Common parser options
    aparser.add_argument('-c',
                         "--config-file",
                         metavar="FILE",
                         # type=argparse.FileType('r', encoding='UTF-8'),
                         type=str,
                         required=True,
                         help="""HTCondor job descriptions file"""
                         )

    aparser.add_argument('-p',
                         "--parent-directory",
                         type=str,
                         required=True,
                         help="""Parent directory in which pool exorciser will set up 
                             crondor jobs and workflow instantiations.""")

    # Operation-specific parsers
    subparsers = aparser.add_subparsers()

    # ######################################3###########
    # Initial setup: directory creation & crondor jobs $
    # ######################################3###########

    # Prepare working directory with workflow files
    crondor_parser = subparsers.add_parser(
        'make-crondor',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        help="Prepare igwn-exorciser directory with workflow files",
        epilog="""Usage example
^^^^^^^^^^^^^
SOME TEXT
            """)

    crondor_parser.set_defaults(which='make-crondor')

    crondor_parser.add_argument('-s',
                                '--submit-cronjobs',
                                action='store_true',
                                help="""Submit Crondor jobs right away""")

    # #####################################
    # Test DAGs generation and submission $
    # #####################################

    # Registration parser: options for registration in rucio catalog
    make_dags_parser = subparsers.add_parser(
        'make-dags',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        help="Generate and submit DAGs.",
        epilog="""Usage example
^^^^^^^^^^^^^
SOME TEXT
        """)

    make_dags_parser.set_defaults(which='make-dags')

    make_dags_parser.add_argument('-s',
                                  '--submit-dags',
                                  action='store_true',
                                  help="""Submit DAG right away""")

    make_dags_parser.add_argument('-i',
                                  "--initial-working-directory-label",
                                  default=datetime.now().strftime("%Y%m%d-%H0000"),
                                  help="""Initial working directory label for workflows.
                         Default: $(date +"%%Y%%m%%d-%%H0000")_{test_name}"""
                                  )

    make_dags_parser.add_argument('-t',
                                  "--test-name",
                                  default='',
                                  help="""Test name from config.yaml whose DAG we will generate.
                                  If not specified, we will generate DAGs for all tests in 
                                  config.yaml."""
                                  )

    make_dags_parser.add_argument('-o',
                                  "--virtual-organisation",
                                  default="IGWN",
                                  choices=["IGWN", "OSG"],
                                  help="""Submit to sites supported by this VO."""
                                  )

    argcomplete.autocomplete(aparser)

    aparser = aparser.parse_args(sys.argv[1:])

    return aparser


def main():
    """Run the thing
    """

    # Parse input
    args = parse_inputs()
    config_data = parse_config_yaml(args.config_file)

    if not os.path.exists(args.parent_directory):
        os.makedirs(args.parent_directory)

    if args.which == 'make-crondor':

        print(f"Preparing igwn-exorciser in {args.parent_directory}")
        prepare_workflows.make_crondor_jobs(args.parent_directory, config_data,
                                            args.submit_cronjobs)

    elif args.which == 'make-dags':

        dag_dir_label = str(os.path.join(args.parent_directory,
                                         args.initial_working_directory_label))

        prepare_workflows.make_test_dags(dag_dir_label, config_data,
                                         args.test_name, args.virtual_organisation,
                                         args.submit_dags)


if __name__ == "__main__":
    sys.exit(main())
