# -*- coding: utf-8 -*-
# Copyright (C) 2024  James Alexander Clark <james.clark@ligo.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Utility to create HTCondor workflows to test/demonstrate
functionality/features in dHTC HTCondor resource pools.  This is the main
driver.
"""

import os

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

from pathlib import Path
import warnings

# disable warnings when condor config source is not found
with warnings.catch_warnings():
    warnings.simplefilter("ignore", UserWarning)
    import htcondor
    import classad

from . import utils
from . import plugins

# FIXME figure out how to do:
# submit_dict["require_gpus"] = f"(DeviceName=!={classad.quote('Tesla K10.G1.8GB')}) && (
# DeviceName=!={classad.quote('Tesla K10.G2.8GB')})"

_QUOTED_ATTRS = ["MY.DESIRED_Sites", "MY.SingularityImage", "environment", "transfer_output_remaps"]
_CRONDOR_PREFIX = "cron-submit"


def make_job(submit_dict: dict, parent_directory: str, test_name: str,
             write_submit_file: bool = False, do_submit: bool = False,
             crondor: bool = False) -> htcondor.Submit:
    """Generate HTCondor job description from YAML config.

    @param submit_dict: dictionary with configuration parameters parsed from YAML.
    @param parent_directory: string with output parent directory for submit files and logs.
    @param test_name: string with name of test to run.
    @param write_submit_file: bool, whether to write submit file (for e.g. Crondor jobs).
    @param do_submit: bool, whether to submit the job directly to the queue.
    @param crondor: bool, whether to generate an HTCondor CronJob; used for naming.
    @return: HTCondor job submit description
    @rtype: htcondor.Submit
    """

    # Determine stdout, stderr, UserLogs
    submit_filename = f"{test_name}.sub"

#   if (("MY.DESIRED_Sites" in submit_dict and submit_dict["MY.DESIRED_Sites"] == "nogrid") or
#           crondor):
#       log_filenames = {key: f"{test_name}-$(cluster).$(process).{ext}"
#                        for key, ext in [("output", "out"), ("error", "err"), ("log", "log")]}
#   else:
#       log_filenames = {key: f"{test_name}-$$(GLIDEIN_Site)-$(cluster).$(process).{ext}"
#                        for key, ext in [("output", "out"), ("error", "err"), ("log", "log")]}

    log_filenames = {key: f"{test_name}-$(cluster).$(process).{ext}"
                     for key, ext in [("output", "out"), ("error", "err"), ("log", "log")]}

    log_dir = os.path.abspath(parent_directory)

    # Set some useful names
    submit_dict["batch_name"] = f"{test_name}-$(cluster).$(process)"

    if crondor:
        submit_filename = f"{_CRONDOR_PREFIX}_{submit_filename}"
        log_filenames = {key:
                f"""{_CRONDOR_PREFIX}_{value.replace('$(process)',
                '$(process)-$(SUBMIT_TIME)')}"""
                for key, value in
                log_filenames.items()}

        submit_dict["erase_output_and_error_on_restart"] = False

        submit_dict["batch_name"] = f"{_CRONDOR_PREFIX}_{test_name}"

    log_pfns = {key: os.path.join(log_dir, log_filenames[key]) for key in log_filenames}
    submit_pfn = os.path.abspath(os.path.join(parent_directory, submit_filename))

    # Construct dictionary with submit commands and values from the YAML configuration
    submit_dict.update(log_pfns)

    # Sanitize:
    # - Add "" quotes around relevant values in submit description commands
    # - Remove queue statement and record desired count
    submit_dict.update({key: classad.quote(value)
                        for key, value in submit_dict.items() if key in _QUOTED_ATTRS})
    if "queue" in submit_dict:
        count = int(submit_dict["queue"])
        _ = submit_dict.pop("queue")
    else:
        count = 1


    submit_description = htcondor.Submit(submit_dict)

    # Required for Crondor jobs; job submit files in DAGMan workflows will be written when the
    # DAG file is  produced
    if write_submit_file:
        with open(submit_pfn, 'w') as fname:
            fname.write(str(submit_description) + f"queue {count}\n")
        print(f'Submit file written to: {submit_pfn}')

    # Submit job via condor_submit.  Use case is crondor jobs; not used in DAGMan workflows
    if do_submit:
        schedd = htcondor.Schedd()
        submit_result = schedd.submit(submit_description, count=count)
        print(f"{submit_pfn} submitted with ClusterID: {submit_result.cluster()}")

    return submit_description


def make_crondor_jobs(parent_directory: str, config: dict, submit_cronjob: bool = False) -> None:
    """Generates all configured HTCondor CronJobs to submit individual DAGMan workflows to test
    different functionality in the IGWN pool.

    @param parent_directory: string with output parent directory for submit files and logs.
    @param config: dictionary with configuration parameters parsed from YAML.
    @param submit_cronjob: boolean to indicate whether to directly submit the jobs
    @return: None
    """

    for pool_test in config:

        # Set up crondor job
        print(f"Setting up crondor job for test: {pool_test}")
        crondor_config = config[pool_test]["crondor"]["submit-description"]

        # Append test name to arguments so that we have separate cronjobs for each test
        # That way we can do say hourly heartbeat tests and daily capacity tests for example
        if crondor_config["arguments"].find("--test-name") == -1:
            crondor_config["arguments"] += f" --test-name {pool_test}"

        crondor_job = make_job(crondor_config, parent_directory, pool_test,
                               write_submit_file=True,
                               do_submit=submit_cronjob,
                               crondor=True)

        print("\n ---- Submit Description ----")
        print(crondor_job)

    return None


def make_test_dags(parent_directory: str, config: dict, test_name: str = '',
                   virtual_organisation: str = "IGWN", submit_dags: bool = False) -> None:
    """Generate the DAGMan workflows associated with each pool functionality test.  Calls out to
    a plugin infrastructure to construct specific DAGMan workflows and dependencies.

    @param parent_directory: string with output parent directory for DAGMan files, submit files
    and logs.
    @param config: dictionary with configuration parameters parsed from YAML.
    @param test_name: string with name of test to generate.
    @param submit_dags: boolean to indicate whether to directly submit DAGMan workflows.
    @return: None
    """

    if test_name == '':
        print("Generating all test workflows in configuration YAML")
    else:
        # Reduce to  just the named test
        reduced_config = {key: value for key, value in config.items() if key == test_name}
        config = reduced_config

    for pool_test in config:

        workflow_directory = f"{parent_directory}_{pool_test}"

        # Set up submit filesgand DAGs for each test
        print("---------------------------------------------")
        print(f"Setting up pool test: {pool_test} in {workflow_directory}")

        # Generate job submit descriptions for this test's DAG generation plugin
        all_jobs = dict()
        for job_config in config[pool_test]["test"]["jobs"]:

            # Prepend the pool test name to the job name to preserve independent sub files
            job_config["job"] = pool_test + "_" + job_config["job"]

            # List of htcondor.Submit objects which we will hand to DAG layers
            all_jobs[job_config["job"]] = make_job(job_config["submit-description"],
                                                   workflow_directory, test_name=job_config["job"])

        # Set up DAGs
        dag_config = config[pool_test]["test"]["dag"]
        dag = plugins[dag_config['plugin']].make_dag(dag_config=dag_config,
                                                     submit_descriptions=all_jobs,
                                                     vo=virtual_organisation,
                                                     parent_directory=os.path.abspath(
                                                         workflow_directory))

        # make the magic happen!
        dag_file = htcondor.dags.write_dag(dag_file_name=f"{pool_test}.dag",
                                           dag_dir=workflow_directory, dag=dag)

        # Fix any FINAL node present
        utils.fix_final_node(Path(dag_file), Path(workflow_directory).resolve())

        print(f"DAG:\n{dag_file}\n{dag.describe()}")

        if submit_dags:

            # Use parent_directory basename as batch-name prefix for DAGMan job proper
            batch_name_prefix = parent_directory.split('/')[-2]
            if batch_name_prefix == '.':
                batch_name = f'{pool_test}.dag+$(ClusterId)'
            else:
                batch_name = f'{batch_name_prefix}-{pool_test}.dag+$(ClusterId)'

            print(f"Submitting with batch name: {batch_name}...")
            dag_submit = htcondor.Submit.from_dag(str(dag_file), 
                    {'force': 1, 'batch-name': batch_name})
            schedd = htcondor.Schedd()
            cluster_id = schedd.submit(dag_submit).cluster()

            print(f"DAGMan job cluster is {cluster_id}")

    return None
