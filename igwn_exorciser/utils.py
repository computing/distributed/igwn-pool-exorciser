#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Common utiltiies for IGWN exorciser
"""

import re
import warnings
from pathlib import Path
with warnings.catch_warnings():
    warnings.simplefilter("ignore", UserWarning)
    import htcondor
    from htcondor import dags


_OSG_FACTORY = "gfactory-2.opensciencegrid.org"
_CERN_FACTORY = "vocms0207.cern.ch"
_TIGER_FACTORY = "gfactory-1.osg-htc.org"

_SUPPORTED_VOS = ["IGWN", "OSG"]

_CONSTRAINTS = {
    "IGWN_CPU": ('MyType=?="glidefactory" && (stringListMember("LIGO", GLIDEIN_Supported_VOs) '
                 '|| stringListMember("VIRGO", GLIDEIN_Supported_VOs))'),
    "IGWN_GPU": ('MyType=?="glidefactory" && (stringListMember("LIGOGPU", '
                 'GLIDEIN_Supported_VOs) || stringListMember("VIRGOGPU", GLIDEIN_Supported_VOs))'),
    "OSG_CPU": ('MyType=?="glidefactory" && (stringListMember("OSGVO", GLIDEIN_Supported_VOs))'),
    "OSG_GPU": ('MyType=?="glidefactory" && (stringListMember("OSGVOGPU", GLIDEIN_Supported_VOs))')
    }


def find_description(submit_descriptions, job_name_pattern):

    pattern = re.compile(r'^.*_{0}$'.format(job_name_pattern))
    matching_key = [key for key in submit_descriptions.keys() if pattern.match(key)]
    assert len(matching_key) == 1, (
        f"Expecting a single job description matching {job_name_pattern}, found "
        f"{len(matching_key)}")

    return matching_key[0]


def get_sites(mode: str, vo: str = "IGWN") -> list[str]:
    """Find sites which support the LIGO or Virgo VOs by querying the OSG and CERN factory
    collectors with constraints on GLIDEIN_Supported_VOs.

    @param mode: string to indicate whether we are looking for CPU (supported VOs: LIGO, Virgo) or
    GPU resources (supported VOs: LIGOGPU, VIRGO).
    @param vo: string to indicate the VO to query for (IGWN or OSG).
    @return: a list of unique GLIDEIN_Site values.
    @rtype: list[str]
    """

    vo = vo.upper()
    mode = mode.upper()

    assert vo in _SUPPORTED_VOS, f"Valid VOs: {_SUPPORTED_VOS}, got {vo}"
    assert mode in ['CPU', 'GPU'], f"Valid modes: ['CPU', 'GPU'], got {mode}"

    constraint = _CONSTRAINTS[f"{vo}_{mode}"]

    glidein_sites = []
    #for collector_address in [_OSG_FACTORY, _CERN_FACTORY]:
    for collector_address in [_TIGER_FACTORY]:
        collector = htcondor.Collector(collector_address)
        machine_ads = collector.query(ad_type=htcondor.htcondor.AdTypes.Any,
                                      constraint=constraint,
                                      projection=['GLIDEIN_Site'])
        glidein_sites += [machine_ad['GLIDEIN_Site']
                          for machine_ad in machine_ads]

    return list(set(glidein_sites))


def flat_dag(submit_descriptions: list[htcondor.htcondor.Submit], submit_vars: dict,
             parent_directory: str) -> dags.DAG:
    """Generate a flat HTCondor DAG with a single layer and no inter-job dependencies: just a
    list of independent jobs we want DAGMan to manage.  Supports multiple job types.

    Provided here as a convenience to import into other DAG-generation plugins.

    @param submit_descriptions: a list of independent HTCondor job submit descriptions.
    @param submit_vars: a dictionary of job argument variable keys and their values.
    @param parent_directory: desired output directory to write DAG and submit files to.
    @return: a DAG boject with a single layer.
    @rtype: dags.DAG
    """
    dag = dags.DAG()

    for submit_description in submit_descriptions:
        # Construct DAG layer
        _ = dag.layer(
            name=f"{submit_description}",
            submit_description=submit_descriptions[submit_description],
            vars=submit_vars,
            dir=parent_directory)

    return dag


def fix_final_node(dag_file_path: Path, parent_directory: Path):
    """Appends the DIR directive to any FINAL node in a DAG which would otherwise
    be ignored"""

    with open(dag_file_path, 'r') as dag_file:
        lines = dag_file.readlines()

    modified_lines = []
    for line in lines:
        if line.startswith("FINAL"):
            new_line = line.strip() + f" DIR {parent_directory}\n"
            modified_lines.append(new_line)
        else:
            modified_lines.append(line)

    with open(dag_file_path, 'w') as dag_file:
        dag_file.writelines(modified_lines)

